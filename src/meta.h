/*
 * meta.h
 * Copyright © 2016 David A. Baer
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the organization nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY David A. Baer ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL David A. Baer BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef _META_H
#define _META_H

#define META_XML_TEMPLATE \
    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" \
    "<office:document-meta xmlns:office=\"urn:oasis:names:tc:opendocument:xmlns:office:1.0\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:meta=\"urn:oasis:names:tc:opendocument:xmlns:meta:1.0\" xmlns:ooo=\"http://openoffice.org/2004/office\" office:version=\"1.2\">\n" \
    "  <office:meta>\n" \
    "    <meta:generator>OpenOffice.org/3.1$Unix OpenOffice.org_project/310m19$Build-9420</meta:generator>\n" \
    "    <dc:title>%s</dc:title>\n" \
    "    <meta:creation-date>%s</meta:creation-date>\n" \
    "    <dc:language>en-US</dc:language>\n" \
    "    <meta:editing-cycles>4</meta:editing-cycles>\n" \
    "    <meta:editing-duration>PT00H11M59S</meta:editing-duration>\n" \
    "    <meta:initial-creator>%s</meta:initial-creator>\n" \
    "    <dc:date>%s</dc:date>\n" \
    "    <dc:creator>%s</dc:creator>\n" \
    "    <meta:document-statistic meta:table-count=\"0\" meta:image-count=\"0\" meta:object-count=\"0\" meta:page-count=\"3\" meta:paragraph-count=\"18\" meta:word-count=\"1420\" meta:character-count=\"7596\"/>\n" \
    "    <meta:user-defined meta:name=\"Info 1\"/>\n" \
    "    <meta:user-defined meta:name=\"Info 2\"/>\n" \
    "    <meta:user-defined meta:name=\"Info 3\"/>\n" \
    "    <meta:user-defined meta:name=\"Info 4\"/>\n" \
    "    <meta:template xlink:type=\"simple\" xlink:actuate=\"onRequest\" xlink:title=\"%s\" xlink:href=\"../../.openoffice.org/3/user/template/default.ott\" meta:date=\"%s\"/>\n" \
    "  </office:meta>\n" \
    "</office:document-meta>\n"

#endif /* !def(_META_H) */
