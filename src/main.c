/*
 * main.c
 * Copyright © 2015 David A. Baer
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the organization nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY David A. Baer ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL David A. Baer BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <libxml/tree.h>
#include <err.h>
#include <time.h>
#include "meta.h"
#include "odt.h"
#include "options.h"
#include "sermon.h"
#include "xml.h"
#include "xslt.h"

extern int yyparse(Sermon *);
extern FILE* yyin;

char*
constructMetaXml(const char* sermonTitle, const char* timestamp,
        const char* sermonAuthor) {
    size_t sz = sizeof(META_XML_TEMPLATE) - 13 + 2 * strlen(sermonTitle)
        + 3 * strlen(timestamp) + 2 * strlen(sermonAuthor);
    char* result = malloc(sz);
    if (!result) {
        perror("malloc");
        exit(1);
    }
    snprintf(result, sz, META_XML_TEMPLATE, sermonTitle, timestamp,
            sermonAuthor, timestamp, sermonAuthor, sermonTitle, timestamp);

    return result;
}

int main(int argc, char* argv[]) {
    Sermon sermon;
    xmlDocPtr document, transformed;
    int i = 0, block = 0, normal = 0;
#ifdef HAVE_PLEDGE
    if (-1 == pledge("stdio rpath wpath tmppath proc exec", NULL)) {
        err(1, "pledge");
    }
#endif /* !def(HAVE_PLEDGE) */
    InitOptions(argc, (const char**)argv);
    InitSermon(&sermon);

    if (strcmp(options.inputFileName, "-") == 0) {
        yyin = stdin;
    } else {
        yyin = fopen(options.inputFileName, "rt");
    }
    yyparse(&sermon);

#if 0
#ifdef HAVE_PLEDGE
    if (-1 == pledge("stdio", NULL)) {
        err(1, "pledge");
    }
#endif /* !def(HAVE_PLEDGE) */
#endif

    document = sermonToXmlDoc(&sermon);
    if (strcmp(options.styleSheetName, "none") != 0) {
        transformed = applyStyleSheet(document, options.styleSheetName);
    } else {
        transformed = document;
    }
    if (strcmp(options.outputFileName, "-") == 0) {
        printXML(transformed);
    } else {
        if ((strcmp(options.styleSheetName, "odt") == 0) ||
            (strcmp(options.styleSheetName, "ms_odt") == 0)) {
            ODTFileEntry entry[2];
            int sz;
            xmlChar* doc;
            char templateFileName[FILENAME_MAX];
            char timestamp[20];
            time_t t;
            struct tm ts;

            /* get timestamp */
            time(&t);
            localtime_r(&t, &ts);
            strftime(timestamp, 20, "%Y-%m-%dT%H:%M:%S", &ts);

            xmlDocDumpMemoryEnc(transformed, &doc, &sz, "utf-8");
            entry[0].path = "content.xml";
            entry[0].content = doc;
            entry[1].path = "meta.xml";
            entry[1].content = constructMetaXml(sermon.sermonTitle, timestamp, sermon.sermonAuthor ? sermon.sermonAuthor : options.authorName);
            snprintf(templateFileName, FILENAME_MAX, "%s.odt", options.styleSheetName);
            constructODT(options.outputFileName, OptionsDataFile(templateFileName), 2, entry);
            free(doc);
            free(entry[1].content);
        } else {
            xmlSaveFileEnc(options.outputFileName, transformed, "utf-8");
        }
    }

    /* clean up, clean up, everybody, everywhere! */
    VERBOSE_PRINTF("Cleaning up\n");
    xmlFreeDoc(document);
    if (strcmp(options.styleSheetName, "none") != 0) {
        xmlFreeDoc(transformed);
    }
    FreeSermon(&sermon);
    if (strcmp(options.inputFileName, "-") != 0) {
        fclose(yyin);
    }
    FreeOptions();
    VERBOSE_PRINTF("Cleanup done\n");
}

