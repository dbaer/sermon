/*
 * dict.h
 * Copyright © 2016 David A. Baer
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the organization nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY David A. Baer ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL David A. Baer BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
#ifndef _DICT_H
#define _DICT_H

#define DICT_OK            0
#define DICT_DUPLICATE_KEY 1
#define DICT_NOT_FOUND     2

#define DICT_OPTION_REPLACE 1

struct dict;
struct dict_iter;

struct dict* dict_init();
uint8_t dict_get_options(const struct dict*);
void dict_set_options(struct dict*, uint8_t);
int dict_add(struct dict*, const char* key, void* value);
int dict_find(const struct dict*, const char* key, void** value);
void dict_free(struct dict*);
void dict_free_cb(struct dict*, void(*)(const char*, void*));
void dict_free_free_value(struct dict*);
unsigned int dict_size(const struct dict*);

/*
 * a dict_iter keeps state for an in-order traversal of the dictionary
 */
const char* dict_iter_key(const struct dict_iter*);
void* dict_iter_value(struct dict_iter*);

struct dict_iter* dict_iter_begin(struct dict*);
void dict_iter_next(struct dict_iter*);
int dict_iter_done(const struct dict_iter*);
void dict_iter_free(struct dict_iter*);

#endif /* !def(_DICT_H) */
