/*
 * options.c
 * Copyright © 2015 David A. Baer
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the organization nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY David A. Baer ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL David A. Baer BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <libgen.h>
#include "options.h"

Options options = {
    .progname = NULL, .datadir = DATADIR, .styleSheetName = NULL,
    .outputFileName = "-", .placeName = "Highlands Presbyterian Church",
    .authorName = "David A. Baer", .verbose = 0
};

char*
datadir(const char* progname) {
    struct stat sb;
    char* progname_copy = strdup(progname);
    const char* progdir = dirname(progname_copy);
    char* local_datadir = NULL;
    char result[PATH_MAX];
    int l = strlen(progdir) + 9;

    if (stat(DATADIR, &sb) == 0) {
        if (S_ISDIR(sb.st_mode)) {
            free(progname_copy);
            return strdup(DATADIR);
        }
    }

    local_datadir = malloc(l);
    snprintf(local_datadir, l, "%s/../data", progdir);
    realpath(local_datadir, result);
    free(local_datadir);
    free(progname_copy);
    if (stat(result, &sb) == 0) {
        if (S_ISDIR(sb.st_mode)) {
            return strdup(result);
        }
    }

    return NULL;
}

static void usage(const char* progname) {
    fprintf(stderr, "Usage: %s [options] FILE\n"
            "\n"
            "Options:\n"
            "    -h|--help                   Display this help message\n"
            "    -a|--author AUTHOR          Author name\n"
            "    -o|--output OUTPUT          Output file name\n"
            "    -s|--stylesheet STYLESHEET  Apply stylesheet (default \"html5\")\n"
            "    -v|--verbose                Verbose output\n"
            "\n"
            "    FILE         sermon file to scan (\"-\" for stdin)\n", progname);
}

void InitOptions(int argc, const char* argv[]) {
    int i = 0;
    options.progname = argv[0];
    options.datadir = datadir(options.progname);
    while (++i < argc) {
        if ((strcmp(argv[i], "-h") == 0) ||
            (strcmp(argv[i], "--help") == 0)) {
            usage(options.progname);
            exit(0);
        } else if (strcmp(argv[i], "-") == 0) {
            options.inputFileName = argv[i];
        } else if ((strcmp(argv[i], "-s") == 0) ||
                   (strcmp(argv[i], "--stylesheet") == 0)) {
            options.styleSheetName = argv[++i];
        } else if (strncmp(argv[i], "-s", 2) == 0) {
            options.styleSheetName = argv[i] + 2;
        } else if ((strcmp(argv[i], "-o") == 0) ||
                   (strcmp(argv[i], "--output") == 0)) {
            options.outputFileName = argv[++i];
        } else if (strncmp(argv[i], "-o", 2) == 0) {
            options.outputFileName = argv[i] + 2;
        } else if ((strcmp(argv[i], "-a") == 0) ||
                   (strcmp(argv[i], "--author") == 0)) {
            options.authorName = argv[++i];
        } else if (strncmp(argv[i], "-a", 2) == 0) {
            options.authorName = argv[i] + 2;
        } else if ((strcmp(argv[i], "-p") == 0) ||
                   (strcmp(argv[i], "--place") == 0)) {
            options.placeName = argv[++i];
        } else if (strncmp(argv[i], "-p", 2) == 0) {
            options.placeName = argv[i] + 2;
        } else if ((strcmp(argv[i], "-v") == 0) ||
                   (strcmp(argv[i], "--verbose") == 0)) {
            options.verbose = 1;
        } else if (argv[i][0] == '-') {
            fprintf(stderr, "Unknown option: %s\n", argv[i]);
        } else {
            options.inputFileName = argv[i];
        }
    }

    /* input filename required */
    if (!options.inputFileName) {
        usage(options.progname);
        exit(1);
    }

    /* set stylesheet based on output file name */
    if (!options.styleSheetName && options.outputFileName) {
        char* outputExten = strrchr(options.outputFileName, '.');
        if (outputExten) {
            if (strcasecmp(outputExten, ".odt") == 0) {
                options.styleSheetName = "odt";
            } else if (strcasecmp(outputExten, ".html") == 0) {
                options.styleSheetName = "html5";
            }
        }
    }

    /* default stylesheet is html5 */
    if (!options.styleSheetName) {
        options.styleSheetName = "html5";
    }
}

char*
OptionsDataFile(const char* fname) {
    char result[PATH_MAX], *t = malloc(strlen(options.datadir) + strlen(fname) + 2);
    snprintf(t, PATH_MAX, "%s/%s", options.datadir, fname);
    realpath(t, result);
    free(t);
    return strdup(result);
}

void FreeOptions() {
    free(options.datadir);
}
