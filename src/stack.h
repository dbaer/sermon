/***************************************************************************
 * stack.h - macros for defining a LIFO stack
 *
 * Copyright (C) 2015 by David A. Baer, All Rights Reserved
 *
 * Description:
 *
 *    These macros are designed to make it easier to process input whose
 *    length is unknown before it terminates (e.g. a series of numbers),
 *    where a last-in, first-out structure is helpful.
 *
 */
#ifndef _STACK_H
#define _STACK_H
#include <stdlib.h>

#define DEFINE_STACK(T, N) \
struct _##N##Node { \
    T data; \
    struct _##N##Node* next; \
};\
typedef struct { \
    int height; \
    struct _##N##Node *head; \
} N

#define NEW_STACK(T, N) \
    T N = (T) { .height = 0, .head = NULL }

#define INIT_STACK(N) { \
    (N).height = 0; \
    (N).head = NULL; \
}
    
#define PUSH_STACK(T, S, E) { \
    struct _##T##Node* n = (struct _##T##Node*)malloc(sizeof(struct _##T##Node)); \
    if (!n) { perror ("Could not allocate space for new stack element."); exit(1); } \
    n->data = E; \
    n->next = (S).head; \
    (S).head = n; \
    (S).height++; \
}

#define STACK_HEIGHT(S) S.height

/* WARNING: YOU MUST VERIFY THAT STACK IS NONEMPTY */
#define STACK_HEAD(S) (S).head->data
#define POP_STACK(T, S) { struct _##T##Node* tmp = (S).head->next; free((S).head); (S).head = tmp; (S).height--; }

#define DESTROY_STACK(T, S) { \
    struct _##T##Node* ptr = (S).head; \
    while (ptr != NULL) { \
        struct _##T##Node* tmp = ptr->next; \
        free(ptr); \
        ptr = tmp; \
    } \
    (S).head = NULL; \
    (S).height = 0; \
}
        
#endif /* !def _STACK_H */

