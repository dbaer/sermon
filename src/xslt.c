#include <stdio.h>
#include <string.h>
#include <libxml/tree.h>
#include <libxslt/xslt.h>
#include <libxslt/transform.h>
#include "options.h"

xmlDocPtr
applyStyleSheet(xmlDocPtr document, const char* styleSheetName) {
    int l = strlen(styleSheetName) + 5;
    char *t = malloc(l), *styleSheetFileName = NULL;
    xsltStylesheetPtr xsl = NULL;
    xmlDocPtr res = NULL;

    snprintf(t, l, "%s.xsl", styleSheetName);
    styleSheetFileName = OptionsDataFile(t);
    free(t);
    /*fprintf(stderr, "Loading stylesheet %s ...\n", styleSheetFileName);*/

    xmlSubstituteEntitiesDefault(1);
    xmlLoadExtDtdDefaultValue = 1;
    xsl = xsltParseStylesheetFile(styleSheetFileName);
    res = xsltApplyStylesheet(xsl, document, NULL);

    free(styleSheetFileName);
    xsltFreeStylesheet(xsl);

    return res;
}
