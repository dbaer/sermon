#ifndef _XSLT_H
#define _XSLT_H

#include <libxml/tree.h>

xmlDocPtr applyStyleSheet(xmlDocPtr document, const char* styleSheetName);

#endif /* !def _XSLT_H */
