%{
#include <string.h>
#include "sermon.h"
#include "sermon_parser.h"

#ifdef LEXDEBUG
#define LEXPRINT(x...) fprintf(stderr, x);
#else
#define LEXPRINT(x...)
#endif
%}
%option noyywrap
%s HEADER HEADERVAL REFERENCEINTRO REFERENCENAME REFERENCE BLOCK
WHITESPACE                   [ \t]
ID                           [A-Za-z_][A-Za-z_0-9]*
%%
<INITIAL>^[[]\*[^*]*\*\]     { /* comment */ LEXPRINT("Comment: %s\n", yytext); }
<INITIAL>^[[]                { BEGIN(HEADER); yylloc.first_column = yylloc.last_column = 1; return '['; }
<HEADER,HEADERVAL>\]         { BEGIN(INITIAL); yylloc.first_column = ++yylloc.last_column; return ']'; }
<HEADER>[^:]*                { yylloc.first_column = yylloc.last_column + 1; yylloc.last_column = yylloc.first_column + strlen(yytext) - 1; yylval.sval = strdup(yytext); return HEADERTYPE; }
<HEADER>:{WHITESPACE}*       { BEGIN(HEADERVAL); yylloc.first_column = yylloc.last_column + 1; yylloc.last_column = yylloc.first_column + strlen(yytext) - 1; return ':'; }
<HEADERVAL>[^\]]*            { yylloc.first_column = yylloc.last_column + 1; yylloc.last_column = yylloc.first_column + strlen(yytext) - 1; yylval.sval = strdup(yytext); return HEADERVALUE; }
<INITIAL>^\{                 { BEGIN(REFERENCEINTRO); yylloc.first_column = yylloc.last_column = 1; return '{'; }
<REFERENCEINTRO>ref          { yylloc.first_column = yylloc.last_column + 1; yylloc.last_column = yylloc.first_column + 2; return KW_REF; }
<REFERENCEINTRO>:            { BEGIN(REFERENCENAME); yylloc.first_column = ++yylloc.last_column; return ':'; }
<REFERENCENAME>{ID}          { yylloc.first_column = yylloc.last_column + 1; yylloc.last_column = yylloc.first_column + strlen(yytext) + 1; yylval.sval = strdup(yytext); return ID; }
<REFERENCENAME>:             { BEGIN(REFERENCE); yylloc.first_column = ++yylloc.last_column; return ':'; }
<REFERENCE>[^}]*             { yylloc.first_column = yylloc.last_column + 1; yylloc.last_column = yylloc.first_column + strlen(yytext) + 1; yylval.sval = strdup(yytext); return REFTEXT; }
<REFERENCE>[}]               { BEGIN(INITIAL); yylloc.first_column = ++yylloc.last_column; return '}'; }
^[^[{>|\n].*                 { yylloc.first_column = ++yylloc.last_column; yylval.sval = strdup(yytext); LEXPRINT("LINE: %s\n", yytext); return LINE; }
^[>|]{WHITESPACE}*\n         { yylloc.first_column = yylloc.last_column = 0; yylloc.first_line++; yylloc.last_line++; return '\n'; }
^[>|]{WHITESPACE}*           {
                                 BEGIN(BLOCK);
                                 yylloc.first_column = yylloc.last_column = 1;
                                 return yytext[0];
                             }
<BLOCK>..*                   {
                                 BEGIN(INITIAL);
                                 yylloc.first_column = yylloc.last_column + 1;
                                 yylloc.last_column = yylloc.first_column + strlen(yytext) - 1;
                                 yylval.sval = strdup(yytext); return LINE;
                             }
<INITIAL>\n                  { yylloc.first_column = yylloc.last_column = 0; yylloc.first_line++; yylloc.last_line++; return '\n'; }
<<EOF>>                      { return 0; }
%%

