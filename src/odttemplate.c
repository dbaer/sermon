/*
 * odttemplate.c
 * Copyright © 2016 David A. Baer
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the organization nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY David A. Baer ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL David A. Baer BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include <archive.h>
#include <archive_entry.h>
#include "odttemplate.h"

#define ODTTEMPLATE_BLOCK_SIZE 8192

int odttemplate(const char* template_fn, const char* output_fn, struct dict* substitutions_dict) {

    struct archive* arch_in = archive_read_new(),
        *arch_out = archive_write_new();
    struct archive_entry* ent;
    struct dict_iter* i;
    char ftype;
    const char* fn;
    if (archive_read_support_format_zip(arch_in) != ARCHIVE_OK) {
        return ODTTEMPLATE_FATAL;
    }
    if (archive_write_set_format_zip(arch_out) != ARCHIVE_OK) {
        return ODTTEMPLATE_FATAL;
    }
    if (archive_write_zip_set_compression_deflate(arch_out) != ARCHIVE_OK) {
        return ODTTEMPLATE_FATAL;
    }
    if (archive_read_open_filename(arch_in, template_fn, ODTTEMPLATE_BLOCK_SIZE) != ARCHIVE_OK) {
        return ODTTEMPLATE_FATAL;
    }
    if (archive_write_open_filename(arch_out, output_fn) != ARCHIVE_OK) {
        return ODTTEMPLATE_FATAL;
    }
    while (archive_read_next_header(arch_in, &ent) != ARCHIVE_EOF) {
        if ((archive_entry_filetype(ent) == AE_IFREG) &&
            (dict_find(substitutions_dict, archive_entry_pathname(ent), NULL)
                 == DICT_OK)) {
        } else {
        }
        archive_read_data_skip(arch_in);
    }
    archive_read_close(arch_in);
    archive_read_free(arch_in);
    i = dict_iter_begin(substitutions_dict);
    while (!dict_iter_done(i)) {
        printf("Substitute %s: \"%s\"\n", dict_iter_key(i), (char*)dict_iter_value(i));
        dict_iter_next(i);
    }

    return ODTTEMPLATE_OK;
}

#ifdef ODTTEMPLATE_DEBUG
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[]) {
    struct dict* d = dict_init();
    if (argc < 2) {
        printf("Specify ZIP file.\n");
        exit(1);
    }
    dict_add(d, "content.xml", "<foo>bar</foo>");
    odttemplate(argv[1], NULL, d);
}
#endif
