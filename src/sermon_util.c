#include <stdlib.h>
#include <string.h>
#include "sermon.h"
#include "format.h"

void InitSermon(Sermon* srm) {
    memset(srm, 0, sizeof(Sermon));
}

void FreeSermon(Sermon* srm) {
    int i = 0;
    if (srm->sermonTitle) free(srm->sermonTitle);
    if (srm->sermonAuthor) free(srm->sermonAuthor);
    if (srm->sermonDate) free(srm->sermonDate);
    if (srm->sermonOccasion) free(srm->sermonOccasion);
    if (srm->sermonText) free(srm->sermonText);

    if (srm->numParagraphs) {
        for (i = 0; i < srm->numParagraphs; i++) {
            freeFormatElementArray(
                srm->sermonParagraphs[i].paraContent,
                srm->sermonParagraphs[i].paraContentLength
            );
        }
        free(srm->sermonParagraphs);
    }
    if (srm->numReferences) {
        for (i = 0; i < srm->numReferences; i++) {
            free(srm->sermonReferences[i].refId);
            freeFormatElementArray(
                srm->sermonReferences[i].refText.paraContent,
                srm->sermonReferences[i].refText.paraContentLength
            );
        }
        free(srm->sermonReferences);
    }
}

