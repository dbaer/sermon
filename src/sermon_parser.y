%{
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "queue.h"
#include "citations.h"
#include "sermon.h"

#ifdef PARSE_DEBUG
#define PARSEPRINT(x...) fprintf(stderr, x);
#else
#define PARSEPRINT(x...)
#endif

int yylex();
extern char* yytext;
extern FILE* yyin;

DEFINE_QUEUE(char *, LineQueue);
DEFINE_QUEUE(SermonParagraph, ParagraphQueue);
DEFINE_QUEUE(SermonReference, ReferenceQueue);

NEW_QUEUE(LineQueue, lineQ);
NEW_QUEUE(ParagraphQueue, paragraphQ);
NEW_QUEUE(ReferenceQueue, referenceQ);
NEW_QUEUE(CitationRecordQueue, citationQ);

char* lineQueueToString(LineQueue* lq) {
    int paraLength = 0, idx = 0;
    char *dest = NULL;
    FOREACH_QUEUE(LineQueue, *lq, ptr)
        paraLength += strlen(ptr->data) + 1;
    FOREACH_QUEUE_END
    paraLength--;
    dest = (char*)malloc(paraLength + 1);
    memset(dest, 0, paraLength + 1);
    FOREACH_QUEUE(LineQueue, *lq, ptr) {
        strncat(dest + idx, ptr->data, paraLength - idx);
        idx += strlen(ptr->data);
        if (ptr->next != NULL) {
            if (dest[idx-1] != '\n')
                dest[idx++] = ' ';
        } else {
            if (dest[idx-1] == '\n')
               idx--;
        }
        free(ptr->data);
    }
    FOREACH_QUEUE_END
    dest[idx] = '\0';
    return dest;
}

char *lineWithBreak(const char *txt) {
    size_t l = strlen(txt);
    char* s = (char*)malloc(l+2);
    strncpy(s, txt, l + 2);
    s[l] = '\n';
    return s;
}

void yyerror(Sermon*, const char*);

%}
%union {
    char* sval;
    char  cval;
}

%token <sval> HEADERTYPE
%token <sval> HEADERVALUE
%token <sval> ID
%token <sval> REFTEXT
%token <sval> LINE
%token KW_REF
%parse-param {Sermon* sermon}
%initial-action {
};
%%
sermon:
      headerlist sermontext references {
          sermon->numParagraphs = QUEUE_LENGTH(paragraphQ);
          sermon->numReferences = QUEUE_LENGTH(citationQ);
          if (sermon->numParagraphs) QUEUE_TO_ARRAY(ParagraphQueue, paragraphQ, SermonParagraph, sermon->sermonParagraphs);
          if (sermon->numReferences) {
              sermon->sermonReferences = calloc(QUEUE_LENGTH(citationQ), sizeof(SermonReference));
              FOREACH_QUEUE(ReferenceQueue, referenceQ, ptr) {
                  int i = lookupCitation(citationQ, ptr->data.refId);
                  if (i) {
                      sermon->sermonReferences[i - 1] = ptr->data;
                  }
              }
              FOREACH_QUEUE_END
          } else {
              sermon->sermonReferences = NULL;
          }
          DESTROY_QUEUE(ParagraphQueue, paragraphQ);
          DESTROY_QUEUE(ReferenceQueue, referenceQ);
          FOREACH_QUEUE(CitationRecordQueue, citationQ, ptr) {
              free(ptr->data.refId);
          }
          FOREACH_QUEUE_END
          DESTROY_QUEUE(CitationRecordQueue, citationQ);
      }
      ;
break:
     break '\n'
   | /* empty */
   ;
headerlist:
          headerlist header break
        | /* empty */
        ;
header:
      '[' HEADERTYPE ':' HEADERVALUE ']'  {
          PARSEPRINT("Parsed header %s\n", $2);
               if (strcmp($2, "title")    == 0) { sermon->sermonTitle    = $4; }
          else if (strcmp($2, "author")   == 0) { sermon->sermonAuthor   = $4; }
          else if (strcmp($2, "text")     == 0) { sermon->sermonText     = $4; }
          else if (strcmp($2, "occasion") == 0) { sermon->sermonOccasion = $4; }
          else if (strcmp($2, "date")     == 0) { sermon->sermonDate     = $4; }
          else if (strcmp($2, "place")    == 0) { sermon->sermonPlace    = $4; }
          else                                  { free($4); }
          free($2);
      }
      ;
sermontext:
          sermontext block break
        | /* empty */
        ;
block:
     para {
         SermonParagraph p = { .paraType = PARA_DEFAULT };
         char* paraText = lineQueueToString(&lineQ);
         FormatElement* paraContent = NULL;
         int paraContentLength = formatText(paraText, &paraContent, &citationQ);
         p.paraContentLength = paraContentLength;
         p.paraContent = paraContent;
         APPEND_QUEUE(ParagraphQueue, paragraphQ, p);
         PARSEPRINT("Parsed paragraph:\n%s\n\n", p.paraText);
         free(paraText);
     }
   | blockquote_or_preserve {
         SermonParagraph p = { .paraType = PARA_BLOCKQUOTE };
         char* paraText = lineQueueToString(&lineQ);
         FormatElement* paraContent = NULL;
         int paraContentLength = formatText(paraText, &paraContent, &citationQ);
         p.paraContentLength = paraContentLength;
         p.paraContent = paraContent;
         APPEND_QUEUE(ParagraphQueue, paragraphQ, p);
         PARSEPRINT("Parsed paragraph:\n%s\n\n", p.paraText);
         free(paraText);
     }
   ;
para:
    para LINE '\n' { APPEND_QUEUE(LineQueue, lineQ, $2); }
  | LINE '\n'      { DESTROY_QUEUE(LineQueue, lineQ); APPEND_QUEUE(LineQueue, lineQ, $1); }
  ;
blockquote_or_preserve:
          blockquote
        | blockpreserve
        ;
blockquote:
          blockquote '>' LINE '\n' { APPEND_QUEUE(LineQueue, lineQ, $3); }
        | '>' LINE '\n' { DESTROY_QUEUE(LineQueue, lineQ); APPEND_QUEUE(LineQueue, lineQ, $2); }
        ;
blockpreserve:
          blockpreserve '|' LINE '\n' { char* s = lineWithBreak($3); APPEND_QUEUE(LineQueue, lineQ, s); free($3); }
        | '|' LINE '\n' { char* s = lineWithBreak($2); DESTROY_QUEUE(LineQueue, lineQ); APPEND_QUEUE(LineQueue, lineQ, s); free($2); }
references:
          references reference break
        | /* empty */
        ;
reference:
         '{' KW_REF ':' ID ':' REFTEXT '}' {
             SermonReference r = { .refId = $4 };
             SermonParagraph p = { .paraType = PARA_DEFAULT };
             FormatElement* paraContent = NULL;
             p.paraContentLength = formatText($6, &p.paraContent, NULL);
             r.refText = p;
             APPEND_QUEUE(ReferenceQueue, referenceQ, r);
         }
        ;
%%

void yyerror(Sermon* s, const char* msg) {
    fprintf(stderr, "Parse error (%d:%d): %s - \"%s\"\n", yylloc.first_line, yylloc.first_column, msg, yytext);
    exit(1);
}
