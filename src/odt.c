/*
 * odt.c
 * Copyright © 2016 David A. Baer
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the organization nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY David A. Baer ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL David A. Baer BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "odt.h"
#include "options.h"

void
saveEntry(const char* rootPath, const ODTFileEntry entry) {
    char* fullpath = malloc(FILENAME_MAX);
    FILE* ptr;
    snprintf(fullpath, FILENAME_MAX, "%s/%s", rootPath, entry.path);
    ptr = fopen(fullpath, "w");
    fwrite(entry.content, 1, strlen(entry.content), ptr);
    fclose(ptr);
    free(fullpath);
}

static void
extractTemplateDocument(const char* const templateFilename, const char* const tempDir) {
    pid_t child_pid;
    if ((child_pid = fork()) == 0) {
        /* you are the child */
        char* const args[] = { "unzip", "-d", tempDir, templateFilename, NULL };
        freopen("/dev/null", "w", stdout);
        execvp("unzip", args);
        perror("execvp");
        exit(1);
    } else {
        /* you are the parent */
        int status;

        waitpid(child_pid, &status, 0);
        if (status != 0) {
            fprintf(stderr, "unzip exited with status %d.\n", status);
            exit(1);
        }
    }
}

static void
createOutputDocument(const char* const outputFilename, const char* const tempDir) {
    pid_t child_pid;
    if ((child_pid = fork()) == 0) {
        /* you are the child */
        char outputRealPath[FILENAME_MAX];
        char* const args[] = { "zip", "-r", outputRealPath, "mimetype", ".", NULL };
        realpath(outputFilename, outputRealPath);
        chdir(tempDir);
        freopen("/dev/null", "w", stdout);
        execvp("zip", args);
        perror("execvp");
        exit(1);
    } else {
        /* you are the parent */
        int status;

        waitpid(child_pid, &status, 0);
        if (status != 0) {
            fprintf(stderr, "zip exited with status %d.\n", status);
            exit(1);
        }
    }
}

static void
removeDirectory(const char* const tempDir) {
    pid_t child_pid;
    if ((child_pid = fork()) == 0) {
        /* you are the child */
        char* const args[] = { "rm", "-rf", tempDir, NULL };
        freopen("/dev/null", "w", stdout);
        execvp("rm", args);
        perror("execvp");
        exit(1);
    } else {
        /* you are the parent */
        int status;

        waitpid(child_pid, &status, 0);
        if (status != 0) {
            fprintf(stderr, "rm exited with status %d.\n", status);
            exit(1);
        }
    }
}

int
constructODT(const char* const outputFilename, const char* const templateFilename,
        unsigned int numEntries, const ODTFileEntry* entries) {
    char tempDir[23];
    int i;

    /* create temporary workspace */
    strcpy(tempDir, "/tmp/sermon.XXXXXXXXXX");
    if (mkdtemp(tempDir) == NULL) {
        perror("mkdtemp");
        exit(1);
    }
    VERBOSE_PRINTF("Making temporary directory %s\n", tempDir);

    /* extract template document to workspace */
    VERBOSE_PRINTF("Extracting template %s to temporary directory\n", templateFilename);
    extractTemplateDocument(templateFilename, tempDir);

    /* replace/add entries */
    for (i = 0; i < numEntries; i++) {
        VERBOSE_PRINTF("Adding %s\n", entries[i].path);
        saveEntry(tempDir, entries[i]);
    }

    /* create output file */
    VERBOSE_PRINTF("Creating output file %s\n", outputFilename);
    createOutputDocument(outputFilename, tempDir);

    /* clean up */
    VERBOSE_PRINTF("Removing %s\n", tempDir);
    removeDirectory(tempDir);

    VERBOSE_PRINTF("ODT output successfully created\n");
    return 0;
}
