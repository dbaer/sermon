/*
 * hash.h
 * Copyright © 2015 David A. Baer
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the organization nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY David A. Baer ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL David A. Baer BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */

#ifndef _HASH_H
#define _HASH_H

#include "queue.h"

#define HASH_ENTRIES 1024
uint32_t HASH_KEY(char *);

#define DEFINE_HASH(T, N) \
    typedef struct { \
        char* key; \
        T data; \
    } N##Entry; \
    DEFINE_QUEUE(N##Entry, N##CollisionQueue) \
    typedef N##CollisionQueue N[HASH_ENTRIES]; \
    const N##Entry* N##Find(N tbl, const char* key) { \
        uint32_t track = HASH_KEY(key); \
        FOREACH_QUEUE(N##CollisionQueue, tbl[track], iter) { \
            if (strcmp(key, iter->data.key) == 0) { \
                return &iter->data; \
            } \
        } \
        return NULL; \
    }

#define INIT_HASH(T, H) { \
    int i; \
    for (i = 0; i < HASH_ENTRIES; i++) { \
        (H)[i].length = 0; \
        (H)[i].tail = (H)[i].head = NULL; \
    } \
}



#endif /* !def _HASH_H */
