<?xml version="1.0" encoding="us-ascii"?>
<xsl:transform
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:ser="urn:david-sermon"
 xmlns:fo="http://www.w3.org/1999/XSL/Format"
 version="1.0">

 <xsl:output method="xml" indent="yes"/>

 <xsl:template match="ser:link">
     <fo:inline text-decoration="underline"><xsl:apply-templates select="*|text()"/></fo:inline>
 </xsl:template>

 <xsl:template match="ser:br">
   <fo:line-break />
 </xsl:template>

 <xsl:template match="ser:em">
   <fo:inline font-style="italic"><xsl:apply-templates select="*|text()"/></fo:inline>
 </xsl:template>

 <xsl:template match="ser:strong">
   <fo:inline font-weight="bold"><xsl:apply-templates select="*|text()"/></fo:inline>
 </xsl:template>

 <xsl:template match="ser:greek">
   <fo:inline font-family="PalatinoLinotype"><xsl:apply-templates select="*|text()"/></fo:inline>
 </xsl:template>

 <xsl:template match="ser:hebrew">
   <fo:inline font-family="David"><xsl:apply-templates select="*|text()"/></fo:inline>
 </xsl:template>

 <xsl:template match="ser:unicode">
   <fo:inline font-family="DejaVuSerif"><xsl:apply-templates select="*|text()"/></fo:inline>
 </xsl:template>

 <xsl:template match="ser:cite"></xsl:template>

 <xsl:template match="ser:ref"></xsl:template>

 <!-- match block-level elements in body -->
 <xsl:template match="ser:p">
   <fo:block text-indent="0.2in"><xsl:if test="@xml:space='preserve'"><xsl:attribute name="white-space">pre</xsl:attribute></xsl:if><xsl:apply-templates select="*|text()"/></fo:block>
 </xsl:template>

 <xsl:template match="ser:quote">
   <fo:block border="2px grey solid" font-size="10pt" padding="0.1in" space-before="0.1in" space-after="0.1in" line-height="125%" margin-left="0.1in" margin-right="0.1in"><xsl:apply-templates select="*|text()"/></fo:block>
 </xsl:template>

 <!-- match toplevel element -->
 <xsl:template match="/ser:sermon">
   <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <fo:layout-master-set>
     <fo:simple-page-master
         master-name="kindle"
         page-width="3.6in"
         page-height="4.8in"
         margin="0.3in">
        <!-- Page template goes here -->
      <fo:region-body   margin="0.3in 0.3in"  />
     </fo:simple-page-master>
    </fo:layout-master-set>

    <fo:page-sequence master-reference="kindle" font-family="Palatino Linotype, Times, serif">
     <!-- fo:static-content flow-name="xsl-region-before" margin-left="0.5in" margin-right="0.5in">
      <fo:block text-align="right" font-size="12pt">Page <fo:page-number/></fo:block>
     </fo:static-content -->

     <fo:flow flow-name="xsl-region-body" line-height="200%" font-size="10pt">
      <fo:block text-align="center" font-size="10pt" font-weight="bold" space-after="0.25in"><xsl:value-of select="ser:header/ser:title"/></fo:block>

      <xsl:apply-templates select="ser:body/*" />

     </fo:flow>
     
    </fo:page-sequence>
   </fo:root>

 </xsl:template>

</xsl:transform>
