<?xml version="1.0" encoding="UTF-8"?>

<xsl:transform
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:ser="urn:david-sermon"
 xmlns:office="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
 xmlns:style="urn:oasis:names:tc:opendocument:xmlns:style:1.0"
 xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
 xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
 xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
 xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
 xmlns:xlink="http://www.w3.org/1999/xlink"
 xmlns:dc="http://purl.org/dc/elements/1.1/"
 xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0"
 xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"
 xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
 xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0"
 xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0"
 xmlns:math="http://www.w3.org/1998/Math/MathML"
 xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0"
 xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0"
 xmlns:ooo="http://openoffice.org/2004/office"
 xmlns:ooow="http://openoffice.org/2004/writer"
 xmlns:oooc="http://openoffice.org/2004/calc"
 xmlns:dom="http://www.w3.org/2001/xml-events"
 xmlns:xforms="http://www.w3.org/2002/xforms"
 xmlns:xsd="http://www.w3.org/2001/XMLSchema"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xmlns:rpt="http://openoffice.org/2005/report"
 xmlns:of="urn:oasis:names:tc:opendocument:xmlns:of:1.2"
 xmlns:rdfa="http://docs.oasis-open.org/opendocument/meta/rdfa#"
 xmlns:field="urn:openoffice:names:experimental:ooo-ms-interop:xmlns:field:1.0" 
 xmlns:formx="urn:openoffice:names:experimental:ooxml-odf-interop:xmlns:form:1.0"
 version="1.0">

 <xsl:output method="xml" indent="yes"/>

 <xsl:template match="ser:link">
     <text:a xlink:type="simple" xlink:href=""><xsl:attribute name="xlink:href"><xsl:value-of select="@href"/></xsl:attribute><text:span text:style-name="T2"><xsl:apply-templates match="*|text()"/></text:span></text:a>
 </xsl:template>

 <xsl:template match="ser:br">
   <text:line-break/>
 </xsl:template>

 <xsl:template match="ser:em">
  <text:span text:style-name="T6"><xsl:apply-templates select="*|text()"/></text:span>
 </xsl:template>

 <xsl:template match="ser:strong">
   <text:span text:style-name="T4"><xsl:apply-templates select="*|text()"/></text:span>
 </xsl:template>

 <xsl:template match="ser:greek">
     <text:span text:style-name="T7"><xsl:apply-templates select="*|text()"/></text:span>
 </xsl:template>

 <xsl:template match="ser:hebrew">
     <text:span text:style-name="T3"><xsl:apply-templates select="*|text()"/></text:span>
 </xsl:template>

 <xsl:template match="ser:unicode">
     <text:span text:style-name="T9"><xsl:apply-templates select="*|text()"/></text:span>
 </xsl:template>

 <xsl:template name="do-footnote">
     <xsl:param name="number"/>
     <xsl:apply-templates select="/ser:sermon/ser:footer/ser:ref[@number=$number]/*|/ser:sermon/ser:footer/ser:ref[@number=$number]/text()"/>
 </xsl:template>

 <xsl:template match="ser:cite">
     <text:note text:note-class="footnote"><xsl:attribute name="text:id">ftn<xsl:value-of select="@number"/></xsl:attribute><text:note-citation><xsl:value-of select="@number"/></text:note-citation><text:note-body><text:p text:style-name="Footnote"><xsl:call-template name="do-footnote"><xsl:with-param name="number" select="@number"/></xsl:call-template></text:p></text:note-body></text:note>
 </xsl:template>

 <xsl:template match="ser:ref">
     <!-- empty (handled by ser:cite template) -->
 </xsl:template>

 <!-- match block-level elements in body -->
 <xsl:template match="ser:p" mode="body">
     <text:p text:style-name="Text_20_body"><xsl:apply-templates select="*|text()"/></text:p>
 </xsl:template>

 <xsl:template match="ser:p" mode="quote">
     <text:p>
         <xsl:choose>
             <xsl:when test="not(preceding-sibling::*)">
                 <xsl:attribute name="text:style-name">P1</xsl:attribute>
             </xsl:when>
             <xsl:otherwise>
                 <xsl:attribute name="text:style-name">Quotations</xsl:attribute>
             </xsl:otherwise>
         </xsl:choose>
         <xsl:apply-templates select="*|text()"/>
     </text:p>
 </xsl:template>

 <xsl:template match="ser:quote" mode="body">
   <xsl:apply-templates mode="quote" select="*|text()"/>
 </xsl:template>

 <!-- match toplevel element -->
 <xsl:template match="/ser:sermon">
<office:document-content office:version="1.2">
  <office:scripts/>
  <office:font-face-decls>
    <style:font-face style:name="Tahoma1" svg:font-family="Tahoma"/>
    <style:font-face style:name="Calibri1" svg:font-family="Calibri" style:font-adornments="Bold" style:font-pitch="variable"/>
    <style:font-face style:name="Calibri" svg:font-family="Calibri" style:font-adornments="Regular" style:font-pitch="variable"/>
    <style:font-face style:name="Code2000" svg:font-family="Code2000" style:font-pitch="variable"/>
    <style:font-face style:name="Code2001" svg:font-family="Code2001" style:font-pitch="variable"/>
    <style:font-face style:name="David" svg:font-family="David" style:font-pitch="variable"/>
    <style:font-face style:name="Bitstream Vera Sans" svg:font-family="'Bitstream Vera Sans'" style:font-family-generic="roman" style:font-pitch="variable"/>
    <style:font-face style:name="DejaVu Sans" svg:font-family="'DejaVu Sans'" style:font-family-generic="swiss" style:font-pitch="variable"/>
    <style:font-face style:name="Bitstream Vera Sans1" svg:font-family="'Bitstream Vera Sans'" style:font-family-generic="system" style:font-pitch="variable"/>
    <style:font-face style:name="Tahoma" svg:font-family="Tahoma" style:font-family-generic="system" style:font-pitch="variable"/>
  </office:font-face-decls>
  <office:automatic-styles>
    <style:style style:name="P1" style:family="paragraph" style:parent-style-name="Quotations">
      <style:paragraph-properties fo:margin-top="0.125in" fo:margin-bottom="0.125in"/>
    </style:style>
    <style:style style:name="T1" style:family="text">
      <style:text-properties fo:font-style="normal" style:font-style-asian="normal" style:font-style-complex="normal"/>
    </style:style>
    <style:style style:name="T2" style:family="text">
      <style:text-properties fo:font-style="normal" fo:font-weight="normal" style:font-style-asian="normal" style:font-weight-asian="normal" style:font-style-complex="normal" style:font-weight-complex="normal"/>
    </style:style>
    <style:style style:name="T3" style:family="text">
      <style:text-properties fo:font-style="normal" fo:font-weight="normal" style:font-style-asian="normal" style:font-weight-asian="normal" style:font-name-complex="David" style:font-style-complex="normal" style:font-weight-complex="normal"/>
    </style:style>
    <style:style style:name="T4" style:family="text">
      <style:text-properties fo:font-weight="bold" style:font-weight-asian="bold" style:font-weight-complex="bold"/>
    </style:style>
    <style:style style:name="T5" style:family="text">
      <style:text-properties fo:font-weight="normal" style:font-weight-asian="normal" style:font-weight-complex="normal"/>
    </style:style>
    <style:style style:name="T6" style:family="text">
      <style:text-properties fo:font-style="italic" fo:font-weight="normal" style:font-style-asian="italic" style:font-weight-asian="normal" style:font-style-complex="italic" style:font-weight-complex="normal"/>
    </style:style>
    <style:style style:name="T7" style:family="text">
      <style:text-properties style:font-name="DejaVu Sans" fo:font-style="normal" fo:font-weight="normal" style:font-style-asian="normal" style:font-weight-asian="normal" style:font-style-complex="normal" style:font-weight-complex="normal"/>
    </style:style>
    <style:style style:name="T8" style:family="text">
      <style:text-properties style:font-name="Code2001" fo:font-style="normal" fo:font-weight="normal" style:font-style-asian="normal" style:font-weight-asian="normal" style:font-style-complex="normal" style:font-weight-complex="normal"/>
    </style:style>
    <style:style style:name="T9" style:family="text">
      <style:text-properties style:font-name="DejaVu Sans" fo:font-style="normal" fo:font-weight="normal" style:font-style-asian="normal" style:font-weight-asian="normal" style:font-style-complex="normal" style:font-weight-complex="normal"/>
    </style:style>
  </office:automatic-styles>
  <office:body>
    <office:text>
      <office:forms form:automatic-focus="false" form:apply-design-mode="false"/>
      <text:sequence-decls>
        <text:sequence-decl text:display-outline-level="0" text:name="Illustration"/>
        <text:sequence-decl text:display-outline-level="0" text:name="Table"/>
        <text:sequence-decl text:display-outline-level="0" text:name="Text"/>
        <text:sequence-decl text:display-outline-level="0" text:name="Drawing"/>
      </text:sequence-decls>
      <text:p text:style-name="Title"><xsl:value-of select="ser:header/ser:title"/></text:p>
      <text:p text:style-name="Author"><xsl:value-of select="ser:header/ser:author"/></text:p>
      <text:p text:style-name="Liturgical_20_Occasion"><xsl:value-of select="ser:header/ser:occasion"/></text:p>
      <text:p text:style-name="Date"><xsl:value-of select="ser:header/ser:date"/></text:p>
      <text:p text:style-name="Text">Text: <text:span text:style-name="T1"><xsl:value-of select="ser:header/ser:text"/></text:span></text:p>

      <xsl:apply-templates select="ser:body/*" mode="body"/>

    </office:text>
  </office:body>
</office:document-content>
</xsl:template>
</xsl:transform>
