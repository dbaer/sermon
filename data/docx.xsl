<?xml version="1.0" encoding="UTF-8"?>

<xsl:transform
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:ser="urn:david-sermon"
 xmlns:ve="http://schemas.openxmlformats.org/markup-compatibility/2006"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
 xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
 xmlns:v="urn:schemas-microsoft-com:vml"
 xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
 xmlns:w10="urn:schemas-microsoft-com:office:word"
 xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
 xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
 version="1.0">

 <xsl:output method="xml" indent="yes"/>

 <xsl:template match="ser:link">
     <text:a xlink:type="simple" xlink:href=""><xsl:attribute name="xlink:href"><xsl:value-of select="@href"/></xsl:attribute><text:span text:style-name="T2"><xsl:apply-templates match="*|text()"/></text:span></text:a>
 </xsl:template>

 <xsl:template match="ser:br">
   <text:line-break/>
 </xsl:template>

 <xsl:template match="ser:em">
  <text:span text:style-name="T6"><xsl:apply-templates select="*|text()"/></text:span>
 </xsl:template>

 <xsl:template match="ser:strong">
   <text:span text:style-name="T4"><xsl:apply-templates select="*|text()"/></text:span>
 </xsl:template>

 <xsl:template match="ser:greek">
     <text:span text:style-name="T7"><xsl:apply-templates select="*|text()"/></text:span>
 </xsl:template>

 <xsl:template match="ser:hebrew">
     <text:span text:style-name="T3"><xsl:apply-templates select="*|text()"/></text:span>
 </xsl:template>

 <xsl:template match="ser:unicode">
     <text:span text:style-name="T9"><xsl:apply-templates select="*|text()"/></text:span>
 </xsl:template>

 <xsl:template name="do-footnote">
     <xsl:param name="number"/>
     <xsl:apply-templates select="/ser:sermon/ser:footer/ser:ref[@number=$number]/*|/ser:sermon/ser:footer/ser:ref[@number=$number]/text()"/>
 </xsl:template>

 <xsl:template match="ser:cite">
     <text:note text:note-class="footnote"><xsl:attribute name="text:id">ftn<xsl:value-of select="@number"/></xsl:attribute><text:note-citation><xsl:value-of select="@number"/></text:note-citation><text:note-body><text:p text:style-name="Footnote"><xsl:call-template name="do-footnote"><xsl:with-param name="number" select="@number"/></xsl:call-template></text:p></text:note-body></text:note>
 </xsl:template>

 <xsl:template match="ser:ref">
     <!-- empty (handled by ser:cite template) -->
 </xsl:template>

 <!-- match block-level elements in body -->
 <xsl:template match="ser:p" mode="body">
    <w:p w:rsidR="001D323B" w:rsidRPr="00496608" w:rsidRDefault="001D323B">
      <w:pPr>
        <w:pStyle w:val="BodyText"/>
      </w:pPr>
      <w:r w:rsidRPr="00496608">
          <w:t><xsl:apply-templates select="*|text()"/></w:t>
      </w:r>
    </w:p>
 </xsl:template>

 <xsl:template match="ser:p" mode="quote">
     <text:p>
         <xsl:choose>
             <xsl:when test="not(preceding-sibling::*)">
                 <xsl:attribute name="text:style-name">P1</xsl:attribute>
             </xsl:when>
             <xsl:otherwise>
                 <xsl:attribute name="text:style-name">Quotations</xsl:attribute>
             </xsl:otherwise>
         </xsl:choose>
         <xsl:apply-templates select="*|text()"/>
     </text:p>
 </xsl:template>

 <xsl:template match="ser:quote" mode="body">
   <xsl:apply-templates mode="quote" select="*|text()"/>
 </xsl:template>

 <xsl:template name="header">
     <xsl:param name="style"/>
     <xsl:param name="content"/>
     <w:p w:rsid="001D323B" w:rsidRDefault="001D323B">
         <w:pPr>
             <w:pStyle w:val="$style"/>
         </w:pPr>
         <w:r>
             <w:t><xsl:value-of select="$content"/></w:t>
         </w:r>
     </w:p>
 </xsl:template>

 <!-- match toplevel element -->
 <xsl:template match="/ser:sermon">
     <w:document>
         <w:body>
             <xsl:call-template name="header">
                 <xsl:with-param name="style">Title</xsl:with-param>
                 <xsl:with-param name="content"><xsl:value-of select="ser:header/ser:title"/></xsl:with-param>
             </xsl:call-template>
             <xsl:call-template name="header">
                 <xsl:with-param name="style">Author</xsl:with-param>
                 <xsl:with-param name="content"><xsl:value-of select="ser:header/ser:author"/></xsl:with-param>
             </xsl:call-template>
             <xsl:call-template name="header">
                 <xsl:with-param name="style">LiturgicalOccasion</xsl:with-param>
                 <xsl:with-param name="content"><xsl:value-of select="ser:header/ser:occasion"/></xsl:with-param>
             </xsl:call-template>
             <xsl:call-template name="header">
                 <xsl:with-param name="style">Date</xsl:with-param>
                 <xsl:with-param name="content"><xsl:value-of select="ser:header/ser:date"/></xsl:with-param>
             </xsl:call-template>
             <w:p w:rsidR="001D323B" w:rsidRPr="00496608" w:rsidRDefault="001D323B">
               <w:pPr>
                 <w:pStyle w:val="Text"/>
                 <w:rPr>
                   <w:i w:val="0"/>
                   <w:iCs w:val="0"/>
                 </w:rPr>
               </w:pPr>
               <w:r w:rsidRPr="00496608">
                 <w:t xml:space="preserve">Text: </w:t>
               </w:r>
               <w:r w:rsidRPr="00496608">
                 <w:rPr>
                   <w:i w:val="0"/>
                   <w:iCs w:val="0"/>
                 </w:rPr>
                 <w:t><xsl:value-of select="ser:header/ser:text"/></w:t>
               </w:r>
             </w:p>
             <xsl:apply-templates select="*|text()"/>
             <w:sectPr w:rsidR="001D323B" w:rsidRPr="00496608" w:rsidSect="00496608">
               <w:footerReference w:type="default" r:id="rId6"/>
               <w:footerReference w:type="first" r:id="rId7"/>
               <w:pgSz w:w="10080" w:h="12240" w:orient="landscape" w:code="5"/>
               <w:pgMar w:top="720" w:right="720" w:bottom="720" w:left="720" w:header="720" w:footer="1440" w:gutter="0"/>
               <w:cols w:space="720"/>
             </w:sectPr>
         </w:body>
     </w:document>
 </xsl:template>
</xsl:transform>
