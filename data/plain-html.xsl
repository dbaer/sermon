<?xml version="1.0" encoding="utf-8"?>
<xsl:transform
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:ser="urn:david-sermon"
 exclude-result-prefixes="ser"
 version="1.0">

 <xsl:output method="xml" doctype-public="-//W3C//DTD XHTML 1.1//EN"
             doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"
             cdata-section-elements="style" indent="yes" encoding="utf-8" />

 <xsl:variable name="refnum">0</xsl:variable>

 <xsl:template match="ser:link" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
     <xsl:element name="a" namespace="http://www.w3.org/1999/xhtml">
         <xsl:attribute name="href">
             <xsl:value-of select="@href" />
         </xsl:attribute>
         <xsl:apply-templates select="*|text()"/>
     </xsl:element>
 </xsl:template>

 <xsl:template match="ser:br" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <br />
 </xsl:template>

 <xsl:template match="ser:em" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <em><xsl:apply-templates select="*|text()"/></em>
 </xsl:template>

 <xsl:template match="ser:strong" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <strong><xsl:apply-templates select="*|text()"/></strong>
 </xsl:template>

 <xsl:template match="ser:greek" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <span class="greek"><xsl:apply-templates select="*|text()"/></span>
 </xsl:template>

 <xsl:template match="ser:unicode" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <span class="unicode"><xsl:apply-templates select="*|text()"/></span>
 </xsl:template>

 <xsl:template match="ser:cite" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <xsl:element name="a" namespace="http://www.w3.org/1999/xhtml">
    <xsl:attribute name="class">ref</xsl:attribute>
    <xsl:attribute name="href">#ref<xsl:value-of select="@number"/></xsl:attribute>
    <xsl:value-of select="@number"/>
   </xsl:element>
 </xsl:template>

 <xsl:template match="ser:ref" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <li class="reference">
   <xsl:attribute name="id">ref<xsl:value-of select="@number" /></xsl:attribute>
   <xsl:apply-templates select="*|text()"/>
   </li>
 </xsl:template>

 <!-- match block-level elements in body -->
 <xsl:template match="ser:p" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <xsl:choose>
     <xsl:when test="@xml:space='preserve'"><pre><xsl:apply-templates select="*|text()"/></pre></xsl:when>
     <xsl:otherwise>
       <p><xsl:apply-templates select="*|text()"/></p>
     </xsl:otherwise>
   </xsl:choose>
 </xsl:template>

 <xsl:template match="ser:quote" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <blockquote><xsl:apply-templates select="*|text()"/></blockquote>
 </xsl:template>

 <!-- match toplevel element -->
 <xsl:template match="/ser:sermon" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <html xmlns="http://www.w3.org/1999/xhtml">
     <head>
       <title><xsl:value-of select="ser:header/ser:title"/></title>
     </head>
     <body>
       <div id="header">
       <h1 id="title"><xsl:value-of select="ser:header/ser:title"/></h1>
       <h2 id="author"><xsl:value-of select="ser:header/ser:author"/></h2>
       <h3 id="occasion"><xsl:value-of select="ser:header/ser:occasion"/></h3>
       <h3 id="place"><xsl:value-of select="ser:header/ser:place"/></h3>
       <h3 id="date"><xsl:value-of select="ser:header/ser:date"/></h3>
       </div>

       <div id="body">
       <p id="text"><em>Text:</em> <xsl:value-of select="ser:header/ser:text"/></p>

       <xsl:apply-templates select="ser:body/*" />

       </div>

       <xsl:if test="count(ser:footer/ser:ref) &gt; 0">
       <div id="footer">
         <ol class="references">
           <xsl:apply-templates select="ser:footer/ser:ref" />
         </ol>
       </div>
       </xsl:if>

     </body>
   </html>
 </xsl:template>

</xsl:transform>
