<?xml version="1.0" encoding="us-ascii"?>
<xsl:transform
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:lit="urn:david-liturgy"
 xmlns:fo="http://www.w3.org/1999/XSL/Format"
 version="1.0">

 <xsl:output method="xml" indent="yes"/>

 <xsl:template match="lit:em">
   <fo:inline font-style="italic"><xsl:apply-templates select="*|text()"/></fo:inline>
 </xsl:template>

 <xsl:template match="lit:strong">
   <fo:inline font-weight="bold"><xsl:apply-templates select="*|text()"/></fo:inline>
 </xsl:template>

 <!-- match block-level elements in body -->
 <xsl:template match="lit:p">
    <fo:block space-after="0.75em" font-size="18pt"><xsl:apply-templates select="*|text()"/></fo:block>
 </xsl:template>

 <xsl:template match="lit:quote">
   <fo:block space-start="0.5in" space-end="0.5in"><xsl:apply-templates select="*|text()"/></fo:block>
 </xsl:template>

 <xsl:template match="lit:header">
   <fo:block space-after="1em" font-weight="bold" font-size="18pt"><xsl:apply-templates select="*|text()"/></fo:block>
 </xsl:template>

 <xsl:template match="lit:part">
   <xsl:apply-templates select="*" />
 </xsl:template>

 <!-- match toplevel element -->
 <xsl:template match="/lit:liturgy">
   <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <fo:layout-master-set>
     <fo:simple-page-master
         master-name="letter"
         page-width="8.5in"
         page-height="11in"
         margin="0.5in">
        <!-- Page template goes here -->
      <fo:region-body   margin="0.5in"  />
      <fo:region-after  extent="0.5in" margin="0in" />
     </fo:simple-page-master>
    </fo:layout-master-set>

    <fo:page-sequence master-reference="letter" font-family="Arial, sans-serif">
     <!-- fo:static-content flow-name="xsl-region-after">
      <fo:block text-align="center" font-size="16pt">Page <fo:page-number/></fo:block>
     </fo:static-content -->

     <fo:flow flow-name="xsl-region-body" text-align="left">

      <xsl:apply-templates select="*">
      </xsl:apply-templates>

     </fo:flow>
     
    </fo:page-sequence>
   </fo:root>

 </xsl:template>

</xsl:transform>
