<?xml version="1.0" encoding="us-ascii"?>
<xsl:transform
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:ser="urn:david-sermon"
 xmlns:fo="http://www.w3.org/1999/XSL/Format"
 version="1.0">

 <xsl:output method="xml" indent="yes"/>

 <xsl:template match="ser:br">
   <fo:line-break />
 </xsl:template>

 <xsl:template match="ser:em">
   <fo:inline font-style="italic"><xsl:apply-templates select="*|text()"/></fo:inline>
 </xsl:template>

 <xsl:template match="ser:strong">
   <fo:inline font-weight="bold"><xsl:apply-templates select="*|text()"/></fo:inline>
 </xsl:template>

 <xsl:template match="ser:greek">
   <fo:inline font-family="PalatinoLinotype"><xsl:apply-templates select="*|text()"/></fo:inline>
 </xsl:template>

 <xsl:template match="ser:hebrew">
   <fo:inline font-family="David"><xsl:apply-templates select="*|text()"/></fo:inline>
 </xsl:template>

 <xsl:template match="ser:unicode">
   <fo:inline font-family="DejaVuSerif"><xsl:apply-templates select="*|text()"/></fo:inline>
 </xsl:template>

 <xsl:template match="ser:cite">
   <fo:inline vertical-align="super" font-size="66%">&#xa0;<xsl:value-of select="@number"/></fo:inline>
 </xsl:template>

 <xsl:template match="ser:ref">
   <fo:list-item font-size="12pt">
     <fo:list-item-label>
       <fo:block line-height="150%" font-size="10pt" text-indent="0.125in"><xsl:value-of select="@number"/>.</fo:block>
     </fo:list-item-label>
     <fo:list-item-body>
         <xsl:apply-templates select="*|text()" mode="reference" />
     </fo:list-item-body>
   </fo:list-item>
 </xsl:template>

 <!-- match block-level elements in body -->
 <xsl:template match="ser:p" mode="body">
    <fo:block line-height="150%" text-indent="0.5in"><xsl:apply-templates select="*|text()"/></fo:block>
 </xsl:template>

 <xsl:template match="ser:p" mode="quote">
   <fo:block line-height="125%" font-size="11pt" text-indent="0" space-after="0.125in"><xsl:if test="@xml:space='preserve'"><xsl:attribute name="white-space">pre</xsl:attribute></xsl:if><xsl:apply-templates select="*|text()"/></fo:block>
 </xsl:template>

 <xsl:template match="ser:p" mode="reference">
   <fo:block line-height="150%" font-size="10pt" margin-left="0.4in" text-indent="-0.125in" margin-bottom="0.125in" text-align="left"><xsl:apply-templates select="*|text()"/></fo:block>
 </xsl:template>

 <xsl:template match="ser:quote" mode="body">
   <fo:block margin-left="0.5in" margin-right="0.5in" space-before="0.125in" space-after="0.125in"><xsl:apply-templates select="*|text()" mode="quote"/></fo:block>
 </xsl:template>

 <!-- match toplevel element -->
 <xsl:template match="/ser:sermon">
   <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format">
    <fo:layout-master-set>
     <fo:simple-page-master
         master-name="letter"
         page-width="8.5in"
         page-height="11in"
         margin="0.5in">
        <!-- Page template goes here -->
      <fo:region-body   margin="0.5in"  />
      <fo:region-after  extent="0.5in" margin="0in" />
     </fo:simple-page-master>
    </fo:layout-master-set>

    <fo:page-sequence master-reference="letter" font-family="'Times New Roman', serif">
     <fo:static-content flow-name="xsl-region-after">
      <fo:block text-align="center" font-size="16pt">Page <fo:page-number/></fo:block>
     </fo:static-content>

     <fo:flow flow-name="xsl-region-body" text-align="justify">
      <fo:block text-align="center" font-size="20pt" font-weight="bold" space-after="0.25in"><xsl:value-of select="ser:header/ser:title"/></fo:block>
      <fo:block text-align="center" font-size="16pt" space-after="0.25in"><xsl:value-of select="ser:header/ser:author"/></fo:block>

      <fo:block text-align="center" font-size="16pt" font-style="italic"><xsl:value-of select="ser:header/ser:place"/></fo:block>
      <fo:block text-align="center" font-size="16pt" font-style="italic"><xsl:value-of select="ser:header/ser:occasion"/></fo:block>
      <fo:block text-align="center" font-size="16pt" font-style="italic"><xsl:value-of select="ser:header/ser:date"/></fo:block>

      <fo:block line-height="150%" space-before="1em" space-after="1em"><fo:inline font-style="italic">Text:</fo:inline>&#xa0;<xsl:value-of select="ser:header/ser:text"/></fo:block>

      <xsl:apply-templates select="ser:body/*" mode="body">
      </xsl:apply-templates>

      <fo:block margin-top="1in">
        <xsl:if test="count(ser:footer/ser:ref) > 0">
        <fo:leader leader-length="2in" leader-pattern="rule" rule-thickness="0.1mm" rule-style="solid" color="black"/>
        <fo:list-block>
          <xsl:apply-templates select="ser:footer/ser:ref" />
        </fo:list-block>
        </xsl:if>
      </fo:block>

     </fo:flow>
     
    </fo:page-sequence>
   </fo:root>

 </xsl:template>

</xsl:transform>
