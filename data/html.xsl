<?xml version="1.0" encoding="utf-8"?>
<xsl:transform
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:ser="urn:david-sermon"
 exclude-result-prefixes="ser"
 version="1.0">

 <xsl:output method="xml" doctype-public="-//W3C//DTD XHTML 1.1//EN"
             xmlns:html="http://www.w3.org/1999/xhtml"
             doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"
             cdata-section-elements="html:style"
             indent="yes" encoding="utf-8" />

 <xsl:variable name="refnum">0</xsl:variable>

 <xsl:template match="ser:link" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
     <xsl:element name="a" namespace="http://www.w3.org/1999/xhtml">
         <xsl:attribute name="href">
             <xsl:value-of select="@href" />
         </xsl:attribute>
         <xsl:apply-templates select="*|text()"/>
     </xsl:element>
 </xsl:template>

 <xsl:template match="ser:br" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <br />
 </xsl:template>

 <xsl:template match="ser:em" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <em><xsl:apply-templates select="*|text()"/></em>
 </xsl:template>

 <xsl:template match="ser:strong" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <strong><xsl:apply-templates select="*|text()"/></strong>
 </xsl:template>

 <xsl:template match="ser:greek" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <span class="greek" xml:lang="grc"><xsl:apply-templates select="*|text()"/></span>
 </xsl:template>

 <xsl:template match="ser:unicode" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <span class="unicode"><xsl:apply-templates select="*|text()"/></span>
 </xsl:template>

 <xsl:template match="ser:cite" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <xsl:element name="a" namespace="http://www.w3.org/1999/xhtml">
    <xsl:attribute name="class">ref</xsl:attribute>
    <xsl:attribute name="href">#ref<xsl:value-of select="@number"/></xsl:attribute>
    <xsl:value-of select="@number"/>
   </xsl:element>
 </xsl:template>

 <xsl:template match="ser:ref" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <li class="reference">
   <xsl:attribute name="id">ref<xsl:value-of select="@number" /></xsl:attribute>
   <xsl:apply-templates select="*|text()"/>
   </li>
 </xsl:template>

 <!-- match block-level elements in body -->
 <xsl:template match="ser:p" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <xsl:choose>
     <xsl:when test="@xml:space='preserve'"><pre><xsl:apply-templates select="*|text()"/></pre></xsl:when>
     <xsl:otherwise>
       <p><xsl:apply-templates select="*|text()"/></p>
     </xsl:otherwise>
   </xsl:choose>
 </xsl:template>

 <xsl:template match="ser:quote" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <blockquote><xsl:apply-templates select="*|text()"/></blockquote>
 </xsl:template>

 <!-- match toplevel element -->
 <xsl:template match="/ser:sermon" xmlns="http://www.w3.org/1999/xhtml" xmlns:ser="urn:david-sermon">
   <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US">
     <head>
       <title><xsl:value-of select="ser:header/ser:title"/></title>
       <style type="text/css"><![CDATA[
    html {
      background-color: #46597D;
      color: white;
    }

    body {
      font-family: "Palatino Linotype", serif;
      font-size: 12pt;
    }

    #header, #body, #footer {
      width: 700px;
      background: white;
      color: black;
      position: relative;
      margin: 0 auto;
      padding: 6px 2em;
    }

    #header h1, #header h2, #header h3 {
      text-align: center;
    }

    #header h2 {
      font-size: 12pt;
      font-weight: normal;
      margin: 15px 0 15px 0;
    }

    #header h3 {
      font-weight: normal;
      font-style: italic;
      font-size: 12pt;
      margin-top: 0;
      margin-bottom: 0;
    }

    #body p {
      line-height: 150%;
      text-indent: 30px;
      text-align: justify;
    }

    #body p#text {
      text-indent: 0;
    }

    a.ref {
      vertical-align: super;
      font-size: 50%;
    }

    #body blockquote {
      margin-left: 30px;
      margin-right: 30px;
      background: #ccc;
      padding: 6px;
      font-size: 11pt;
    }

    #body blockquote p {
      line-height: 100%;
    }

    #footer li {
      font-size: 10pt;
    }

    ]]></style>
     </head>
     <body>
       <div id="header">
       <h1 id="title"><xsl:value-of select="ser:header/ser:title"/></h1>
       <h2 id="author"><xsl:value-of select="ser:header/ser:author"/></h2>
       <h3 id="occasion"><xsl:value-of select="ser:header/ser:occasion"/></h3>
       <h3 id="place"><xsl:value-of select="ser:header/ser:place"/></h3>
       <h3 id="date"><xsl:value-of select="ser:header/ser:date"/></h3>
       </div>

       <div id="body">
       <p id="text"><em>Text:</em>&#xa0;<xsl:value-of select="ser:header/ser:text"/></p>

       <xsl:apply-templates select="ser:body/*" />

       </div>

       <xsl:if test="count(ser:footer/ser:ref) &gt; 0">
       <div id="footer">
         <ol class="references">
           <xsl:apply-templates select="ser:footer/ser:ref" />
         </ol>
       </div>
       </xsl:if>

     </body>
   </html>
 </xsl:template>

</xsl:transform>
