sermon 1.4
==========

This utility converts text markup into various presentable forms.

Usage
-----

    Usage: sermon [options] FILE

    Options:
        -h|--help                   Display this help message
        -a|--author AUTHOR          Author name
        -o|--output OUTPUT          Output file name
        -s|--stylesheet STYLESHEET  Apply stylesheet (default "html5")
        -v|--verbose                Verbose output

        FILE         sermon file to scan ("-" for stdin)


Markup Syntax
-------------
The markup syntax is very simple:

  - Provide a header with title and other information at the top:

        [title: My Document Title]
        [author: My Name]
        [date: January 1, 2015]
        [occasion: New Year's Day]
        [text: Luke 2:21-40]

    Note that all fields are optional.

  - Leave a blank line between paragraphs.

  - Use `*asterisks*` for *emphasis* (rendered in italics)

  - Use `^{notename}` within the text for a footnote/endnote reference.
    Provide the footnote below your text with

        {ref:notename:Author, *Title*. etc...}

  - For an offset block quotation, prepend > to each line of a paragraph:

        > This paragraph will be
        > a block quote.

  - Text enclosed between `[* starred braces *]` will be ignored as a comment.
